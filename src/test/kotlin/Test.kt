import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import romannumber.RomanNumberTranslator

class Test {
    lateinit var roman: RomanNumberTranslator

    @BeforeEach
    fun before() {
        roman = RomanNumberTranslator()
    }

    @Test
    fun `known roman numbers`() {
        assertEquals("I", roman.fromArabic(1))
        assertEquals("V", roman.fromArabic(5))
        assertEquals("X", roman.fromArabic(10))
        assertEquals("L", roman.fromArabic(50))
        assertEquals("C", roman.fromArabic(100))
        assertEquals("D", roman.fromArabic(500))
        assertEquals("M", roman.fromArabic(1000))
    }

    @Test
    fun `known roman number plus one`() {
        assertEquals("II", roman.fromArabic(2))
        assertEquals("VI", roman.fromArabic(6))
        assertEquals("XI", roman.fromArabic(11))
        assertEquals("LI", roman.fromArabic(51))
        assertEquals("CI", roman.fromArabic(101))
        assertEquals("DI", roman.fromArabic(501))
        assertEquals("MI", roman.fromArabic(1001))
    }

    @Test
    fun `known roman number plus two`() {
        assertEquals("III", roman.fromArabic(3))
        assertEquals("VII", roman.fromArabic(7))
        assertEquals("XII", roman.fromArabic(12))
        assertEquals("LII", roman.fromArabic(52))
        assertEquals("CII", roman.fromArabic(102))
        assertEquals("DII", roman.fromArabic(502))
        assertEquals("MII", roman.fromArabic(1002))
    }

    @Test
    fun `known romans minus one`() {
        assertEquals("IV", roman.fromArabic(4))
        assertEquals("IX", roman.fromArabic(9))
        assertEquals("XLIX", roman.fromArabic(49))
        assertEquals("XCIX", roman.fromArabic(99))
        assertEquals("CDXCIX", roman.fromArabic(499))
        assertEquals("CMXCIX", roman.fromArabic(999))
    }

    @Test
    fun `14 - 'XIV'`() {
        assertEquals("XIV", roman.fromArabic(14))
    }

    @Test
    fun `2838 - 'MMDCCCXXXVIII'`() {
        assertEquals("MMDCCCXXXVIII", roman.fromArabic(2838))
    }

    @Test
    fun `2699 - 'MMDCXCIX'`() {
        assertEquals("MMDCXCIX", roman.fromArabic(2699))
    }

    @Test
    fun `40 - XL`() {
        assertEquals("XL", roman.fromArabic(40))
    }

    @Test
    fun `more than 3000 should fail`() {
        assertThrows<RomanNumberTranslator.MaxNumberReached> { roman.fromArabic(3001) }
    }

    @Test
    fun `60 - 'LX'`() {
        assertEquals("LX", roman.fromArabic(60))
    }

    @Test
    fun `known romans to decimal`() {
        assertEquals(1, roman.toArabic("I"))
        assertEquals(5, roman.toArabic("V"))
        assertEquals(10, roman.toArabic("X"))
        assertEquals(50, roman.toArabic("L"))
        assertEquals(100, roman.toArabic("C"))
        assertEquals(500, roman.toArabic("D"))
        assertEquals(1000, roman.toArabic("M"))
    }

    @Test
    fun `LX - 60`() {
        assertEquals(60, roman.toArabic("LX"))
    }

    @Test
    fun `XL - 40`() {
        assertEquals(40, roman.toArabic("XL"))
    }

    @Test
    fun `XLV - 45`() {
        assertEquals(45, roman.toArabic("XLV"))
    }

    @Test
    fun `'MMDCXCIX'- 2699 `() {
        assertEquals(2699, roman.toArabic("MMDCXCIX"))
    }
}