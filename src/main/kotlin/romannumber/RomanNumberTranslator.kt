package romannumber

import java.util.*

class RomanNumberTranslator {
    private val knownRomanNumbers = listOf(
        RomanNumber(1000, "M"),
        RomanNumber(900, "CM"),
        RomanNumber(500, "D"),
        RomanNumber(400, "CD"),
        RomanNumber(100, "C"),
        RomanNumber(90, "XC"),
        RomanNumber(50, "L"),
        RomanNumber(40, "XL"),
        RomanNumber(10, "X"),
        RomanNumber(9, "IX"),
        RomanNumber(5, "V"),
        RomanNumber(4, "IV"),
        RomanNumber(1, "I")
    )

    fun fromArabic(arabic: Int): String {
        if (arabic > 3000)
            throw MaxNumberReached()
        return generateRoman(arabic)
    }

    private fun generateRoman(arabic: Int): String {
        for (romanNumber in knownRomanNumbers)
            if (arabic >= romanNumber.arabic)
                return romanNumber.roman + generateRoman(arabic - romanNumber.arabic)
        return ""
    }

    fun toArabic(roman: String): Int {
        var sum = 0
        for (romanNumber in parseRomanNumbers(roman))
            sum += romanNumber.arabic
        return sum
    }

    private fun parseRomanNumbers(roman: String): Sequence<RomanNumber> {
        return sequence {
            val romanCharacters = roman.toCollection(ArrayDeque(roman.length))
            while (romanCharacters.isNotEmpty())
                yield(parseCharacters(romanCharacters))
        }
    }

    private fun parseCharacters(romanCharacters: ArrayDeque<Char>): RomanNumber {
        val a = romanCharacters.poll()
        val b = romanCharacters.peek()
        val roman = if (isCompoundRoman(a, b)) "$a$b".also { romanCharacters.poll() } else "$a"
        return knownRomanNumbers.find { it.roman == roman }!!
    }

    private fun isCompoundRoman(a: Char?, b: Char?): Boolean {
        return a == 'I' && b in listOf('V', 'X') ||
                a == 'X' && b in listOf('L', 'C') ||
                a == 'C' && b in listOf('D', 'M')
    }

    class MaxNumberReached : Exception()

    private class RomanNumber(val arabic: Int, val roman: String)
}
